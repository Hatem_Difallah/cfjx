package fr.hardis.test;

import org.junit.Assert;
import org.junit.Test;

import fr.cfjx.services.ReadFileService;

public class ReadFileServiceTest {

	@Test
	public void testvalidatorColor() {
		Assert.assertTrue(ReadFileService.isColor("R"));
		Assert.assertFalse(ReadFileService.isColor("H"));
	}

	@Test
	public void testvalidatorNumReference() {
		Assert.assertTrue(ReadFileService.isNumReference("1111111111"));
		Assert.assertFalse(ReadFileService.isNumReference("987"));
	}

	@Test
	public void testvalidatorPrice() {
		Assert.assertTrue(ReadFileService.isPrice("22.00"));
		Assert.assertFalse(ReadFileService.isPrice("KK"));
	}

	@Test
	public void testvalidatorSize() {
		Assert.assertTrue(ReadFileService.isSize("50"));
		Assert.assertFalse(ReadFileService.isSize("FGVHDSB"));
	}
}
