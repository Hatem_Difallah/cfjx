package fr.cfjx.constants;

/***
 * 
 * @author HDHIFALLAH
 *
 */
public class MessageConst {

	public static final String INCORRECT_NUMBER_REFERENCE = "Incorrect value for numReference";
	public static final String INCORRECT_PRICE = "Incorrect value for price";
	public static final String INCORRECT_COLOR = "Incorrect value for color";
	public static final String INCORRECT_SIZE = "Incorrect value for size";
	public static final String ERROR_BUILD_RAPPORT = "Erreur in buildRapport";
	public static final String ERROR_GENERATE_XML = "Erreur in generate xml";
	public static final String ERROR_BUILD_FILE = "Erreur in buildFile";
	public static final String VIEW_LABEL_1 = "Entrer le chemin du fichier � traiter :";
	public static final String VIEW_LABEL_2 = "Entrer le chemin du fichier g�n�rer :";
	public static final String VIEW_LABEL_3 = "S�lectionner le type du fichier � g�n�rer :";
	public static final String VIEW_SUBMIT = "Soumettre";
	public static final String VIEW_TITLE = "G�n�rateur de fichier";
	public static final String SERIF = "Serif";
	public static final String MSG_VIEW_FILE_GENERATE = "Fichier g�n�rer";
	public static final String MSG_VIEW_ERROR_IN_GENERATION = "Erreur en cours de g�n�ration du Fichier";

}
