package fr.cfjx.constants;

/***
 * Les differents couleurs :<br>
 * R : Rouge<br>
 * G : Vert<br>
 * B : Bleu<br>
 * 
 * @author HDHIFALLAH
 * 
 */
public enum ColorEnum {
	R, G, B;
}
