package fr.cfjx.constants;
/***
 * 
 * @author HDHIFALLAH
 *
 */
public class ApplicationConst {

	public static final String FILE = "file.";
	public static final String JSON = "json";
	public static final String PT_VIRGULE = ";";
	public static final String REG_EXP_COLOR = "[RGB]";
	public static final String REG_EXP_NUM_REFERENCE = "[0-9]{10}?";
	public static final String REG_EXP_PRICE = "[0-9]+([,.][0-9]{1,2})?";

}
