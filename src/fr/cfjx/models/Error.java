package fr.cfjx.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/***
 * 
 * @author HDHIFALLAH
 *
 */
@XmlRootElement
public class Error implements Serializable {

	private static final long serialVersionUID = 4001893672529649732L;

	private String line;
	private String message;
	private String value;

	public String getLine() {
		return line;
	}

	@XmlAttribute
	public void setLine(String line) {
		this.line = line;
	}

	public String getMessage() {
		return message;
	}

	@XmlAttribute
	public void setMessage(String message) {
		this.message = message;
	}

	public String getValue() {
		return value;
	}

	@XmlElement
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Error [line=" + line + ", message=" + message + ", value=" + value + "]";
	}

}
