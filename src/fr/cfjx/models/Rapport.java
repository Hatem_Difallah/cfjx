package fr.cfjx.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/***
 * 
 * @author HDHIFALLAH
 *
 */
@XmlRootElement
@XmlType(propOrder = { "inputFile", "references", "errors" })
public class Rapport implements Serializable {

	private static final long serialVersionUID = 7984335181983484853L;

	private String inputFile;
	private List<Reference> references;
	private List<Error> errors;

	public Rapport() {
		super();
		this.references = new ArrayList<>();
		this.errors = new ArrayList<>();
	}

	public String getInputFile() {
		return inputFile;
	}

	@XmlElement
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public List<Reference> getReferences() {
		return references;
	}

	@XmlElementWrapper
	@XmlElement(name = "reference")
	public void setReferences(List<Reference> references) {
		this.references = references;
	}

	public List<Error> getErrors() {
		return errors;
	}

	@XmlElementWrapper
	@XmlElement(name = "error")
	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "Rapport [inputFile=" + inputFile + ", references=" + references + ", errors=" + errors + "]";
	}

}
