package fr.cfjx.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

/***
 * 
 * @author HDHIFALLAH
 *
 */
public class Reference implements Serializable {

	private static final long serialVersionUID = 72425150570211311L;

	private String numReference;
	private Integer size;
	private Float price;
	private String color;

	public String getNumReference() {
		return numReference;
	}

	@XmlAttribute
	public void setNumReference(String numReference) {
		this.numReference = numReference;
	}

	public Integer getSize() {
		return size;
	}

	@XmlAttribute
	public void setSize(Integer size) {
		this.size = size;
	}

	public Float getPrice() {
		return price;
	}

	@XmlAttribute
	public void setPrice(Float price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	@XmlAttribute
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Reference [numReference=" + numReference + ", size=" + size + ", price=" + price + ", color=" + color + "]";
	}

}
