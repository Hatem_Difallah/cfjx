package fr.cfjx.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.codehaus.jackson.map.ObjectMapper;

import fr.cfjx.constants.ApplicationConst;
import fr.cfjx.constants.ColorEnum;
import fr.cfjx.constants.MessageConst;
import fr.cfjx.models.Error;
import fr.cfjx.models.Rapport;
import fr.cfjx.models.Reference;

/***
 * 
 * @author HDHIFALLAH
 *
 */
public class ReadFileService {

	private static final Logger LOGGER = Logger.getLogger(ReadFileService.class.getName());

	/**
	 * 
	 * @param pathFile
	 * @return
	 * @throws IOException
	 */
	public static Rapport buildRapport(String pathFile) throws IOException {

		Rapport rapport = new Rapport();

		try (BufferedReader br = Files.newBufferedReader(Paths.get(pathFile))) {

			// set nom fichier
			rapport.setInputFile(Paths.get(pathFile).getFileName().toString());

			// lire le fichier ligne par ligne
			String line;
			int numberOfLine = 1;
			while ((line = br.readLine()) != null) {
				// compter le num�ro de s�parteur par ligine
				long numberOfSplit = line.chars().filter(num -> num == ';').count();
				// tester si on a le nombre excate du s�parteur
				if (Objects.equals(numberOfSplit, 3L)) {

					String[] extractValuesFromLine = line.split(ApplicationConst.PT_VIRGULE);
					String numReference = extractValuesFromLine[0];
					String color = extractValuesFromLine[1];
					String price = extractValuesFromLine[2];
					String size = extractValuesFromLine[3];

					// si toutes les conditions sont vrai
					if (isNumReference(numReference) && isColor(color) && isPrice(price) && isSize(size)) {
						// construire l'objet Reference
						Reference reference = new Reference();
						reference.setNumReference(numReference);
						reference.setColor(color);
						reference.setPrice(Float.parseFloat(price));
						reference.setSize(Integer.parseInt(size));
						// set l'objet Reference dans la list
						rapport.getReferences().add(reference);

					} else {
						// set les erreurs dans la list
						rapport.getErrors().addAll(getListErrosForLine(numberOfLine, line, numReference, color, price, size));
					}

				}

				numberOfLine++;
			}

		} catch (IOException e) {
			LOGGER.warning(MessageConst.ERROR_BUILD_RAPPORT);
			throw new IOException();
		}

		return rapport;
	}

	/***
	 * 
	 * @param rapport
	 * @param fileResult
	 * @throws IOException
	 * @throws JAXBException
	 */
	public static void buildFile(Rapport rapport, String pathFileResult, String typeFile) throws IOException, JAXBException {

		// Si pathFileResult est vide ou typeFile est vide g�n�re une exception
		if (pathFileResult.isEmpty() || typeFile.isEmpty()) {
			throw new IOException();
		}

		// Construire le fichier selon type
		String filePath = pathFileResult + ApplicationConst.FILE + typeFile;
		File file = new java.io.File(filePath);

		if (Objects.equals(typeFile, ApplicationConst.JSON)) {

			// Construire une fichier JSON
			createFileJSON(rapport, file);
		} else {
			// Construire une fichier XML
			createFileXML(rapport, file);
		}

	}

	/***
	 * 
	 * @param rapport
	 * @param file
	 * @throws IOException
	 */
	private static void createFileJSON(Rapport rapport, File file) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(file, rapport);
	}

	/***
	 * 
	 * @param numberOfLine
	 * @param numReference
	 * @param color
	 * @param price
	 * @param size
	 * @return
	 */
	private static List<Error> getListErrosForLine(int numberOfLine, String line, String numReference, String color, String price, String size) {

		List<Error> listErros = new ArrayList<>();

		if (!isNumReference(numReference)) {
			Error error = new Error();
			error.setLine(String.valueOf(numberOfLine));
			error.setMessage(MessageConst.INCORRECT_NUMBER_REFERENCE);
			error.setValue(line);
			listErros.add(error);
		}
		if (!isPrice(price)) {
			Error error = new Error();
			error.setLine(String.valueOf(numberOfLine));
			error.setMessage(MessageConst.INCORRECT_PRICE);
			error.setValue(line);
			listErros.add(error);
		}
		if (!isColor(color)) {
			Error error = new Error();
			error.setLine(String.valueOf(numberOfLine));
			error.setMessage(MessageConst.INCORRECT_COLOR);
			error.setValue(line);
			listErros.add(error);
		}
		if (!isSize(size)) {
			Error error = new Error();
			error.setLine(String.valueOf(numberOfLine));
			error.setMessage(MessageConst.INCORRECT_SIZE);
			error.setValue(line);
			listErros.add(error);
		}

		return listErros;
	}

	/**
	 * 
	 * @param numReference
	 * @return
	 */
	public static boolean isNumReference(String numReference) {

		return numReference.matches(ApplicationConst.REG_EXP_NUM_REFERENCE);
	}

	/***
	 * 
	 * @param price
	 * @return
	 */
	public static boolean isPrice(String price) {

		return price.matches(ApplicationConst.REG_EXP_PRICE);
	}

	/***
	 * 
	 * @param size
	 * @return
	 */
	public static boolean isSize(String size) {
		try {
			Integer.parseInt(size);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/***
	 * 
	 * @param color
	 * @return
	 */
	public static boolean isColor(String color) {

		boolean colorResult = false;
		if (Objects.equals(color, ColorEnum.R.name()) || Objects.equals(color, ColorEnum.B.name()) || Objects.equals(color, ColorEnum.G.name())) {
			colorResult = true;
		}
		return colorResult;
	}

	/***
	 * 
	 * @param rapport
	 * @param file
	 * @throws JAXBException
	 */
	private static void createFileXML(Rapport rapport, File file) throws JAXBException {
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(Rapport.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(rapport, file);

		} catch (JAXBException e) {

			throw new JAXBException(e);
		}
	}

}
