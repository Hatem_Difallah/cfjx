package fr.cfjx.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;

import fr.cfjx.constants.MessageConst;
import fr.cfjx.models.Rapport;
import fr.cfjx.services.ReadFileService;

/***
 * 
 * @author HDHIFALLAH
 *
 */
public class View {

	private static final Logger LOGGER = Logger.getLogger(View.class.getName());

	public View() {
		JFrame f = new JFrame(MessageConst.VIEW_TITLE);
		// button
		JButton b = new JButton(MessageConst.VIEW_SUBMIT);
		b.setBounds(300, 200, 140, 40);

		// label message
		JLabel labelMsgInfo = new JLabel();
		labelMsgInfo.setBounds(100, 250, 450, 40);

		// label 1
		JLabel label1 = new JLabel();
		label1.setText(MessageConst.VIEW_LABEL_1);
		label1.setBounds(10, 20, 500, 100);
		label1.setFont(new Font(MessageConst.SERIF, Font.PLAIN, 15));

		// label 2
		JLabel label2 = new JLabel();
		label2.setText(MessageConst.VIEW_LABEL_2);
		label2.setBounds(10, 60, 500, 100);
		label2.setFont(new Font(MessageConst.SERIF, Font.PLAIN, 15));

		// label 3
		JLabel label3 = new JLabel();
		label3.setText(MessageConst.VIEW_LABEL_3);
		label3.setBounds(10, 100, 500, 100);
		label3.setFont(new Font(MessageConst.SERIF, Font.PLAIN, 15));

		// textfiel 1
		JTextField textfield1 = new JTextField();
		textfield1.setBounds(300, 50, 250, 30);

		// textfiel 1
		JTextField textfield2 = new JTextField();
		textfield2.setBounds(300, 100, 250, 30);

		// ComboBox
		String fileType[] = { "", "json", "xml" };
		JComboBox<String> liste = new JComboBox<>(fileType);

		// taile Jframe
		liste.setSize(50, 50);
		liste.setBounds(300, 150, 250, 40);

		// ajouter les elements au farme
		f.add(label1);
		f.add(textfield1);
		f.add(label2);
		f.add(textfield2);
		f.add(label3);
		f.add(liste);
		f.add(b);
		f.add(labelMsgInfo);
		f.setSize(600, 350);
		f.setLayout(null);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// action du button
		b.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {
					Rapport rapport = ReadFileService.buildRapport(textfield1.getText());
					ReadFileService.buildFile(rapport, textfield2.getText(), liste.getSelectedItem().toString());
					labelMsgInfo.setText(MessageConst.MSG_VIEW_FILE_GENERATE);
					labelMsgInfo.setForeground(Color.BLUE);
					labelMsgInfo.setFont(new Font(MessageConst.SERIF, Font.PLAIN, 25));

				} catch (IOException e) {
					labelMsgInfo.setText(MessageConst.MSG_VIEW_ERROR_IN_GENERATION);
					labelMsgInfo.setForeground(Color.RED);
					labelMsgInfo.setFont(new Font(MessageConst.SERIF, Font.PLAIN, 25));

					LOGGER.warning(MessageConst.ERROR_BUILD_RAPPORT);
				} catch (JAXBException e) {
					labelMsgInfo.setText(MessageConst.ERROR_GENERATE_XML);
					labelMsgInfo.setForeground(Color.RED);
					labelMsgInfo.setFont(new Font(MessageConst.SERIF, Font.PLAIN, 25));

				}
			}
		});
	}

}